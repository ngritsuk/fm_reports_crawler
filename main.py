#!/usr/bin/python
import os
import jwt
import requests
import StringIO
import gzip
import datetime
import time
import csv
import psycopg2

def download_daily_report(reportType, reportSubType, reportFrequency, api_version):
    with open(os.environ['AUTH_KEY_PATH'], 'r') as key_file:
          header = {
            "alg": "ES256",
            "kid": os.environ['KEY_ID'],
            "typ": "JWT"
            }
          try:
            token = jwt.encode({'iss': os.environ['ISSUER_ID'],
                              'exp': int(time.time()) + 20 * 60,
                              'aud': "appstoreconnect-v1"},
                              key_file.read(),
                              algorithm='ES256',
                              headers=header)
            
          except Exception as e:
            print "Access token encoding failed ", e
            quit()

          parameters = {
                "filter[frequency]": reportFrequency,
                "filter[reportSubType]": reportSubType,
                "filter[reportType]": reportType,
                "filter[vendorNumber]": '88015752',
                "filter[version]": api_version,
          }
          HEAD = {'Authorization': 'Bearer ' + token}
          try:
            response = requests.get(os.environ['BASE_URL'], headers=HEAD, params=parameters)
            compressedData = StringIO.StringIO()
            compressedData.write(response.content)
            compressedData.seek(0)
            decompressedData = gzip.GzipFile(fileobj=compressedData, mode='rb')

            result = decompressedData.read()
            output_file_path = "/tmp/DAILY_{0}_{1}_{2}.csv".format(reportType, reportSubType, datetime.datetime.now().strftime('%Y-%b-%d'))
          except Exception as e:
            print response.text
            print "Receiveing report failed ", e
            quit()

          with open(output_file_path, 'w') as result_file:
            result_file.write(result)
          return output_file_path

def check_environment_variables():
  variables_list = ['ISSUER_ID', 'KEY_ID', 'BASE_URL', 'DB_USER', 'DB_PASSWORD', 'DB_NAME', 'DB_HOST', 'AUTH_KEY_PATH']
  if set(variables_list).issubset(set(os.environ.keys())) and os.path.isfile(os.environ['AUTH_KEY_PATH']):
    print "Data import started"
  else:
    print "Please set all required environment variables: ", variables_list
    quit()

def load_data():
    check_environment_variables()
    subscription_event_report = download_daily_report('SUBSCRIPTION_EVENT','SUMMARY', 'DAILY', '1_1')

    with open(subscription_event_report, 'r') as csvfile:
      reader = csv.DictReader(csvfile, delimiter='\t')
      next(reader)
      connection = psycopg2.connect(user=os.environ['DB_USER'],
                                    password=os.environ['DB_PASSWORD'],
                                    host=os.environ['DB_HOST'],
                                    port="5432",
                                    database=os.environ['DB_NAME'])
      yesterday = (datetime.date.today() - datetime.timedelta(1)).strftime("%Y-%m-%d")
      for row in reader:
          try:
              cursor = connection.cursor()
              event_date = datetime.datetime.strptime(row['Event Date'], '%Y-%m-%d').strftime('%Y-%m-%d')
              original_start_date = datetime.datetime.strptime(row['Original Start Date'], '%Y-%m-%d').strftime('%Y-%m-%d')
              days_before_canceling = row['Days Before Canceling'] if row['Days Before Canceling'] not in ['', ' '] else None
              days_canceled = row['Days Canceled'] if row['Days Canceled'] not in ['', ' '] else None
              values = (event_date , row['Event'], row['App Name'], row['App Apple ID'], row['Subscription Name'], row['Subscription Apple ID'], row['Subscription Group ID'], row['Subscription Duration'],row['Introductory Price Type'], row['Introductory Price Duration'], row['Marketing Opt-In'], row['Marketing Opt-In Duration'], row['Preserved Pricing'], row['Proceeds Reason'], row['Consecutive Paid Periods'], row['Original Start Date'], row['Client'], row['Device'], row['State'], row['Country'], row['Previous Subscription Name'],row['Previous Subscription Apple ID'], days_before_canceling, row['Cancellation Reason'], days_canceled, row['Quantity'])
              if event_date == yesterday:
                print values
                cursor.execute("INSERT INTO subscription_event (event_date, event, app_name, app_apple_id, subscription_name,subscription_apple_id,subscription_group_id,subscription_duration, introductory_price_type, introductory_price_duration, marketing_opt_in, marketing_opt_in_duration, preserved_pricing, proceeds_reason, consecutive_paid_periods, original_start_date, client, device, state, country, previous_subscription_name, previous_subscription_apple_id, days_before_canceling, cancellation_reason, days_canceled, quantity) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", values)
                connection.commit()
                return True

          except (Exception, psycopg2.Error) as error :
              if(connection):
                  print("Failed to insert record into subscription_event, {0}").format(error)
                  return false
    if os.path.exists(subscription_event_report):
      os.remove(subscription_event_report)  

def update_status(status, db_connection):
  cursor = db_connection.cursor()
  query = "INSERT INTO analytics_update_status (success_update) VALUES ({0});".format(status)
  print query
  cursor.execute(query)
  db_connection.commit()

def main():
  db_connection = psycopg2.connect(user=os.environ['DB_USER'],
                                    password=os.environ['DB_PASSWORD'],
                                    host=os.environ['DB_HOST'],
                                    port="5432",
                                    database=os.environ['DB_NAME'])
  cursor = db_connection.cursor()
  cursor.execute('SELECT * FROM analytics_update_status ORDER BY latest_update_time')
  update_statuses = cursor.fetchall()
  latest_status = update_statuses[-1]
  if (not latest_status[1]) or (datetime.datetime.now() - latest_status[0]).days > 0:
    result = load_data()
    update_status(result, db_connection)
    
  else:
    print "Update has been already made today"

if __name__ == '__main__':
    main()
