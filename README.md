# FM Reports Crawler

* Python script for getting latest daily apple subscription report and saving data to postgresql database.**
Before running this script make sure that database exists and configured

# Prerequisites
* Set following env variables ISSUER_ID, KEY_ID, BASE_URL, DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, AUTH_KEY_PATH
